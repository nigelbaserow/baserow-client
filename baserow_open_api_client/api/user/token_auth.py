from http import HTTPStatus
from typing import Any, Dict, Optional, Union, cast

import httpx

from ... import errors
from ...client import Client
from ...models.token_auth_response_200 import TokenAuthResponse200
from ...models.token_obtain_pair_with_user import TokenObtainPairWithUser
from ...types import Response


def _get_kwargs(
    *,
    client: Client,
    form_data: TokenObtainPairWithUser,
    multipart_data: TokenObtainPairWithUser,
    json_body: TokenObtainPairWithUser,
) -> Dict[str, Any]:
    url = "{}/api/user/token-auth/".format(client.base_url)

    headers: Dict[str, str] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    json_body.to_dict()

    multipart_data.to_multipart()

    return {
        "method": "post",
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "follow_redirects": client.follow_redirects,
        "data": form_data.to_dict(),
    }


def _parse_response(*, client: Client, response: httpx.Response) -> Optional[Union[Any, TokenAuthResponse200]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = TokenAuthResponse200.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = cast(Any, response.json())
        return response_401
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(*, client: Client, response: httpx.Response) -> Response[Union[Any, TokenAuthResponse200]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Client,
    form_data: TokenObtainPairWithUser,
    multipart_data: TokenObtainPairWithUser,
    json_body: TokenObtainPairWithUser,
) -> Response[Union[Any, TokenAuthResponse200]]:
    """Authenticates an existing user based on their email and their password. If successful, an access
    token and a refresh token will be returned.

    Args:
        multipart_data (TokenObtainPairWithUser):
        json_body (TokenObtainPairWithUser):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, TokenAuthResponse200]]
    """

    kwargs = _get_kwargs(
        client=client,
        form_data=form_data,
        multipart_data=multipart_data,
        json_body=json_body,
    )

    response = httpx.request(
        verify=client.verify_ssl,
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Client,
    form_data: TokenObtainPairWithUser,
    multipart_data: TokenObtainPairWithUser,
    json_body: TokenObtainPairWithUser,
) -> Optional[Union[Any, TokenAuthResponse200]]:
    """Authenticates an existing user based on their email and their password. If successful, an access
    token and a refresh token will be returned.

    Args:
        multipart_data (TokenObtainPairWithUser):
        json_body (TokenObtainPairWithUser):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Any, TokenAuthResponse200]
    """

    return sync_detailed(
        client=client,
        form_data=form_data,
        multipart_data=multipart_data,
        json_body=json_body,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    form_data: TokenObtainPairWithUser,
    multipart_data: TokenObtainPairWithUser,
    json_body: TokenObtainPairWithUser,
) -> Response[Union[Any, TokenAuthResponse200]]:
    """Authenticates an existing user based on their email and their password. If successful, an access
    token and a refresh token will be returned.

    Args:
        multipart_data (TokenObtainPairWithUser):
        json_body (TokenObtainPairWithUser):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, TokenAuthResponse200]]
    """

    kwargs = _get_kwargs(
        client=client,
        form_data=form_data,
        multipart_data=multipart_data,
        json_body=json_body,
    )

    async with httpx.AsyncClient(verify=client.verify_ssl) as _client:
        response = await _client.request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Client,
    form_data: TokenObtainPairWithUser,
    multipart_data: TokenObtainPairWithUser,
    json_body: TokenObtainPairWithUser,
) -> Optional[Union[Any, TokenAuthResponse200]]:
    """Authenticates an existing user based on their email and their password. If successful, an access
    token and a refresh token will be returned.

    Args:
        multipart_data (TokenObtainPairWithUser):
        json_body (TokenObtainPairWithUser):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Any, TokenAuthResponse200]
    """

    return (
        await asyncio_detailed(
            client=client,
            form_data=form_data,
            multipart_data=multipart_data,
            json_body=json_body,
        )
    ).parsed
