from http import HTTPStatus
from typing import Any, Dict, Optional

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.admin_impersonate_user_response_200 import AdminImpersonateUserResponse200
from ...models.baserow_impersonate_auth_token import BaserowImpersonateAuthToken
from ...types import Response


def _get_kwargs(
    *,
    client: AuthenticatedClient,
    form_data: BaserowImpersonateAuthToken,
    multipart_data: BaserowImpersonateAuthToken,
    json_body: BaserowImpersonateAuthToken,
) -> Dict[str, Any]:
    url = "{}/api/admin/users/impersonate/".format(client.base_url)

    headers: Dict[str, str] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    json_body.to_dict()

    multipart_data.to_multipart()

    return {
        "method": "post",
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "follow_redirects": client.follow_redirects,
        "data": form_data.to_dict(),
    }


def _parse_response(*, client: Client, response: httpx.Response) -> Optional[AdminImpersonateUserResponse200]:
    if response.status_code == HTTPStatus.OK:
        response_200 = AdminImpersonateUserResponse200.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(*, client: Client, response: httpx.Response) -> Response[AdminImpersonateUserResponse200]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    form_data: BaserowImpersonateAuthToken,
    multipart_data: BaserowImpersonateAuthToken,
    json_body: BaserowImpersonateAuthToken,
) -> Response[AdminImpersonateUserResponse200]:
    """This endpoint allows staff to impersonate another user by requesting a JWT token and user object.
    The requesting user must have staff access in order to do this. It's not possible to impersonate a
    superuser or staff.

    This is a **premium** feature.

    Args:
        multipart_data (BaserowImpersonateAuthToken): Serializer used for impersonation.
        json_body (BaserowImpersonateAuthToken): Serializer used for impersonation.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[AdminImpersonateUserResponse200]
    """

    kwargs = _get_kwargs(
        client=client,
        form_data=form_data,
        multipart_data=multipart_data,
        json_body=json_body,
    )

    response = httpx.request(
        verify=client.verify_ssl,
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    form_data: BaserowImpersonateAuthToken,
    multipart_data: BaserowImpersonateAuthToken,
    json_body: BaserowImpersonateAuthToken,
) -> Optional[AdminImpersonateUserResponse200]:
    """This endpoint allows staff to impersonate another user by requesting a JWT token and user object.
    The requesting user must have staff access in order to do this. It's not possible to impersonate a
    superuser or staff.

    This is a **premium** feature.

    Args:
        multipart_data (BaserowImpersonateAuthToken): Serializer used for impersonation.
        json_body (BaserowImpersonateAuthToken): Serializer used for impersonation.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        AdminImpersonateUserResponse200
    """

    return sync_detailed(
        client=client,
        form_data=form_data,
        multipart_data=multipart_data,
        json_body=json_body,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    form_data: BaserowImpersonateAuthToken,
    multipart_data: BaserowImpersonateAuthToken,
    json_body: BaserowImpersonateAuthToken,
) -> Response[AdminImpersonateUserResponse200]:
    """This endpoint allows staff to impersonate another user by requesting a JWT token and user object.
    The requesting user must have staff access in order to do this. It's not possible to impersonate a
    superuser or staff.

    This is a **premium** feature.

    Args:
        multipart_data (BaserowImpersonateAuthToken): Serializer used for impersonation.
        json_body (BaserowImpersonateAuthToken): Serializer used for impersonation.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[AdminImpersonateUserResponse200]
    """

    kwargs = _get_kwargs(
        client=client,
        form_data=form_data,
        multipart_data=multipart_data,
        json_body=json_body,
    )

    async with httpx.AsyncClient(verify=client.verify_ssl) as _client:
        response = await _client.request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    form_data: BaserowImpersonateAuthToken,
    multipart_data: BaserowImpersonateAuthToken,
    json_body: BaserowImpersonateAuthToken,
) -> Optional[AdminImpersonateUserResponse200]:
    """This endpoint allows staff to impersonate another user by requesting a JWT token and user object.
    The requesting user must have staff access in order to do this. It's not possible to impersonate a
    superuser or staff.

    This is a **premium** feature.

    Args:
        multipart_data (BaserowImpersonateAuthToken): Serializer used for impersonation.
        json_body (BaserowImpersonateAuthToken): Serializer used for impersonation.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        AdminImpersonateUserResponse200
    """

    return (
        await asyncio_detailed(
            client=client,
            form_data=form_data,
            multipart_data=multipart_data,
            json_body=json_body,
        )
    ).parsed
