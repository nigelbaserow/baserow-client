from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.get_database_table_view_sort_response_400 import GetDatabaseTableViewSortResponse400
from ...models.get_database_table_view_sort_response_404 import GetDatabaseTableViewSortResponse404
from ...models.view_sort import ViewSort
from ...types import Response


def _get_kwargs(
    view_sort_id: int,
    *,
    client: AuthenticatedClient,
) -> Dict[str, Any]:
    url = "{}/api/database/views/sort/{view_sort_id}/".format(client.base_url, view_sort_id=view_sort_id)

    headers: Dict[str, str] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    return {
        "method": "get",
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "follow_redirects": client.follow_redirects,
    }


def _parse_response(
    *, client: Client, response: httpx.Response
) -> Optional[Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = ViewSort.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = GetDatabaseTableViewSortResponse400.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = GetDatabaseTableViewSortResponse404.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Client, response: httpx.Response
) -> Response[Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    view_sort_id: int,
    *,
    client: AuthenticatedClient,
) -> Response[Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]]:
    """Returns the existing view sort if the authorized user has access to the related database's
    workspace.

    Args:
        view_sort_id (int):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]]
    """

    kwargs = _get_kwargs(
        view_sort_id=view_sort_id,
        client=client,
    )

    response = httpx.request(
        verify=client.verify_ssl,
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    view_sort_id: int,
    *,
    client: AuthenticatedClient,
) -> Optional[Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]]:
    """Returns the existing view sort if the authorized user has access to the related database's
    workspace.

    Args:
        view_sort_id (int):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]
    """

    return sync_detailed(
        view_sort_id=view_sort_id,
        client=client,
    ).parsed


async def asyncio_detailed(
    view_sort_id: int,
    *,
    client: AuthenticatedClient,
) -> Response[Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]]:
    """Returns the existing view sort if the authorized user has access to the related database's
    workspace.

    Args:
        view_sort_id (int):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]]
    """

    kwargs = _get_kwargs(
        view_sort_id=view_sort_id,
        client=client,
    )

    async with httpx.AsyncClient(verify=client.verify_ssl) as _client:
        response = await _client.request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    view_sort_id: int,
    *,
    client: AuthenticatedClient,
) -> Optional[Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]]:
    """Returns the existing view sort if the authorized user has access to the related database's
    workspace.

    Args:
        view_sort_id (int):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[GetDatabaseTableViewSortResponse400, GetDatabaseTableViewSortResponse404, ViewSort]
    """

    return (
        await asyncio_detailed(
            view_sort_id=view_sort_id,
            client=client,
        )
    ).parsed
