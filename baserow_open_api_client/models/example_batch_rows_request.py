import json
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

import attr

if TYPE_CHECKING:
    from ..models.example_row_request_serializer_with_user_field_names import (
        ExampleRowRequestSerializerWithUserFieldNames,
    )


T = TypeVar("T", bound="ExampleBatchRowsRequest")


@attr.s(auto_attribs=True)
class ExampleBatchRowsRequest:
    """
    Attributes:
        items (List['ExampleRowRequestSerializerWithUserFieldNames']):
    """

    items: List["ExampleRowRequestSerializerWithUserFieldNames"]
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        items = []
        for items_item_data in self.items:
            items_item = items_item_data.to_dict()

            items.append(items_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "items": items,
            }
        )

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        _temp_items = []
        for items_item_data in self.items:
            items_item = items_item_data.to_dict()

            _temp_items.append(items_item)
        items = (None, json.dumps(_temp_items).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "items": items,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.example_row_request_serializer_with_user_field_names import (
            ExampleRowRequestSerializerWithUserFieldNames,
        )

        d = src_dict.copy()
        items = []
        _items = d.pop("items")
        for items_item_data in _items:
            items_item = ExampleRowRequestSerializerWithUserFieldNames.from_dict(items_item_data)

            items.append(items_item)

        example_batch_rows_request = cls(
            items=items,
        )

        example_batch_rows_request.additional_properties = d
        return example_batch_rows_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
