from typing import Any, Dict, List, Type, TypeVar, Union

import attr

from ..types import UNSET, Unset

T = TypeVar("T", bound="TokenVerifyWithUser")


@attr.s(auto_attribs=True)
class TokenVerifyWithUser:
    """
    Attributes:
        refresh_token (str):
        token (Union[Unset, str]): Deprecated. Use `refresh_token` instead.
    """

    refresh_token: str
    token: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        refresh_token = self.refresh_token
        token = self.token

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "refresh_token": refresh_token,
            }
        )
        if token is not UNSET:
            field_dict["token"] = token

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        refresh_token = (
            self.refresh_token
            if isinstance(self.refresh_token, Unset)
            else (None, str(self.refresh_token).encode(), "text/plain")
        )
        token = self.token if isinstance(self.token, Unset) else (None, str(self.token).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "refresh_token": refresh_token,
            }
        )
        if token is not UNSET:
            field_dict["token"] = token

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        refresh_token = d.pop("refresh_token")

        token = d.pop("token", UNSET)

        token_verify_with_user = cls(
            refresh_token=refresh_token,
            token=token,
        )

        token_verify_with_user.additional_properties = d
        return token_verify_with_user

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
