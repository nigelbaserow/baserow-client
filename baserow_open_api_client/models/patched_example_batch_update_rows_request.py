import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union

import attr

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.example_batch_update_row_request_serializer_with_user_field_names import (
        ExampleBatchUpdateRowRequestSerializerWithUserFieldNames,
    )


T = TypeVar("T", bound="PatchedExampleBatchUpdateRowsRequest")


@attr.s(auto_attribs=True)
class PatchedExampleBatchUpdateRowsRequest:
    """
    Attributes:
        items (Union[Unset, List['ExampleBatchUpdateRowRequestSerializerWithUserFieldNames']]):
    """

    items: Union[Unset, List["ExampleBatchUpdateRowRequestSerializerWithUserFieldNames"]] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        items: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.items, Unset):
            items = []
            for items_item_data in self.items:
                items_item = items_item_data.to_dict()

                items.append(items_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if items is not UNSET:
            field_dict["items"] = items

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        items: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.items, Unset):
            _temp_items = []
            for items_item_data in self.items:
                items_item = items_item_data.to_dict()

                _temp_items.append(items_item)
            items = (None, json.dumps(_temp_items).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if items is not UNSET:
            field_dict["items"] = items

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.example_batch_update_row_request_serializer_with_user_field_names import (
            ExampleBatchUpdateRowRequestSerializerWithUserFieldNames,
        )

        d = src_dict.copy()
        items = []
        _items = d.pop("items", UNSET)
        for items_item_data in _items or []:
            items_item = ExampleBatchUpdateRowRequestSerializerWithUserFieldNames.from_dict(items_item_data)

            items.append(items_item)

        patched_example_batch_update_rows_request = cls(
            items=items,
        )

        patched_example_batch_update_rows_request.additional_properties = d
        return patched_example_batch_update_rows_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
