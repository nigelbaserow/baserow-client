from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

import attr

from ..models.array_formula_type_enum import ArrayFormulaTypeEnum
from ..models.date_format_enum import DateFormatEnum
from ..models.date_time_format_enum import DateTimeFormatEnum
from ..models.formula_type_enum import FormulaTypeEnum
from ..models.number_decimal_places_enum import NumberDecimalPlacesEnum
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.field import Field


T = TypeVar("T", bound="LookupFieldFieldSerializerWithRelatedFields")


@attr.s(auto_attribs=True)
class LookupFieldFieldSerializerWithRelatedFields:
    """
    Attributes:
        id (int):
        table_id (int):
        name (str):
        order (int): Lowest first.
        type (str): The type of the related field.
        read_only (bool): Indicates whether the field is a read only field. If true, it's not possible to update the
            cell value.
        related_fields (List['Field']): A list of related fields which also changed.
        nullable (bool):
        primary (Union[Unset, bool]): Indicates if the field is a primary field. If `true` the field cannot be deleted
            and the value should represent the whole row.
        error (Union[Unset, None, str]):
        date_force_timezone (Union[Unset, None, str]): Force a timezone for the field overriding user profile settings.
        date_time_format (Union[DateTimeFormatEnum, None, Unset]): 24 (14:30) or 12 (02:30 PM)
        date_include_time (Union[Unset, None, bool]): Indicates if the field also includes a time.
        date_show_tzinfo (Union[Unset, None, bool]): Indicates if the time zone should be shown.
        number_decimal_places (Union[None, NumberDecimalPlacesEnum, Unset]): The amount of digits allowed after the
            point.
        array_formula_type (Union[ArrayFormulaTypeEnum, None, Unset]):
        date_format (Union[DateFormatEnum, None, Unset]): EU (20/02/2020), US (02/20/2020) or ISO (2020-02-20)
        through_field_id (Union[Unset, None, int]): The id of the link row field to lookup values for. Will override the
            `through_field_name` parameter if both are provided, however only one is required.
        through_field_name (Union[Unset, None, str]): The name of the link row field to lookup values for.
        target_field_id (Union[Unset, None, int]): The id of the field in the table linked to by the through_field to
            lookup. Will override the `target_field_id` parameter if both are provided, however only one is required.
        target_field_name (Union[Unset, None, str]): The name of the field in the table linked to by the through_field
            to lookup.
        formula_type (Union[Unset, FormulaTypeEnum]):
    """

    id: int
    table_id: int
    name: str
    order: int
    type: str
    read_only: bool
    related_fields: List["Field"]
    nullable: bool
    primary: Union[Unset, bool] = UNSET
    error: Union[Unset, None, str] = UNSET
    date_force_timezone: Union[Unset, None, str] = UNSET
    date_time_format: Union[DateTimeFormatEnum, None, Unset] = UNSET
    date_include_time: Union[Unset, None, bool] = UNSET
    date_show_tzinfo: Union[Unset, None, bool] = UNSET
    number_decimal_places: Union[None, NumberDecimalPlacesEnum, Unset] = UNSET
    array_formula_type: Union[ArrayFormulaTypeEnum, None, Unset] = UNSET
    date_format: Union[DateFormatEnum, None, Unset] = UNSET
    through_field_id: Union[Unset, None, int] = UNSET
    through_field_name: Union[Unset, None, str] = UNSET
    target_field_id: Union[Unset, None, int] = UNSET
    target_field_name: Union[Unset, None, str] = UNSET
    formula_type: Union[Unset, FormulaTypeEnum] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        table_id = self.table_id
        name = self.name
        order = self.order
        type = self.type
        read_only = self.read_only
        related_fields = []
        for related_fields_item_data in self.related_fields:
            related_fields_item = related_fields_item_data.to_dict()

            related_fields.append(related_fields_item)

        nullable = self.nullable
        primary = self.primary
        error = self.error
        date_force_timezone = self.date_force_timezone
        date_time_format: Union[None, Unset, str]
        if isinstance(self.date_time_format, Unset):
            date_time_format = UNSET
        elif self.date_time_format is None:
            date_time_format = None

        elif isinstance(self.date_time_format, DateTimeFormatEnum):
            date_time_format = UNSET
            if not isinstance(self.date_time_format, Unset):
                date_time_format = self.date_time_format.value

        else:
            date_time_format = self.date_time_format

        date_include_time = self.date_include_time
        date_show_tzinfo = self.date_show_tzinfo
        number_decimal_places: Union[None, Unset, int]
        if isinstance(self.number_decimal_places, Unset):
            number_decimal_places = UNSET
        elif self.number_decimal_places is None:
            number_decimal_places = None

        elif isinstance(self.number_decimal_places, NumberDecimalPlacesEnum):
            number_decimal_places = UNSET
            if not isinstance(self.number_decimal_places, Unset):
                number_decimal_places = self.number_decimal_places.value

        else:
            number_decimal_places = self.number_decimal_places

        array_formula_type: Union[None, Unset, str]
        if isinstance(self.array_formula_type, Unset):
            array_formula_type = UNSET
        elif self.array_formula_type is None:
            array_formula_type = None

        elif isinstance(self.array_formula_type, ArrayFormulaTypeEnum):
            array_formula_type = UNSET
            if not isinstance(self.array_formula_type, Unset):
                array_formula_type = self.array_formula_type.value

        else:
            array_formula_type = self.array_formula_type

        date_format: Union[None, Unset, str]
        if isinstance(self.date_format, Unset):
            date_format = UNSET
        elif self.date_format is None:
            date_format = None

        elif isinstance(self.date_format, DateFormatEnum):
            date_format = UNSET
            if not isinstance(self.date_format, Unset):
                date_format = self.date_format.value

        else:
            date_format = self.date_format

        through_field_id = self.through_field_id
        through_field_name = self.through_field_name
        target_field_id = self.target_field_id
        target_field_name = self.target_field_name
        formula_type: Union[Unset, str] = UNSET
        if not isinstance(self.formula_type, Unset):
            formula_type = self.formula_type.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "table_id": table_id,
                "name": name,
                "order": order,
                "type": type,
                "read_only": read_only,
                "related_fields": related_fields,
                "nullable": nullable,
            }
        )
        if primary is not UNSET:
            field_dict["primary"] = primary
        if error is not UNSET:
            field_dict["error"] = error
        if date_force_timezone is not UNSET:
            field_dict["date_force_timezone"] = date_force_timezone
        if date_time_format is not UNSET:
            field_dict["date_time_format"] = date_time_format
        if date_include_time is not UNSET:
            field_dict["date_include_time"] = date_include_time
        if date_show_tzinfo is not UNSET:
            field_dict["date_show_tzinfo"] = date_show_tzinfo
        if number_decimal_places is not UNSET:
            field_dict["number_decimal_places"] = number_decimal_places
        if array_formula_type is not UNSET:
            field_dict["array_formula_type"] = array_formula_type
        if date_format is not UNSET:
            field_dict["date_format"] = date_format
        if through_field_id is not UNSET:
            field_dict["through_field_id"] = through_field_id
        if through_field_name is not UNSET:
            field_dict["through_field_name"] = through_field_name
        if target_field_id is not UNSET:
            field_dict["target_field_id"] = target_field_id
        if target_field_name is not UNSET:
            field_dict["target_field_name"] = target_field_name
        if formula_type is not UNSET:
            field_dict["formula_type"] = formula_type

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.field import Field

        d = src_dict.copy()
        id = d.pop("id")

        table_id = d.pop("table_id")

        name = d.pop("name")

        order = d.pop("order")

        type = d.pop("type")

        read_only = d.pop("read_only")

        related_fields = []
        _related_fields = d.pop("related_fields")
        for related_fields_item_data in _related_fields:
            related_fields_item = Field.from_dict(related_fields_item_data)

            related_fields.append(related_fields_item)

        nullable = d.pop("nullable")

        primary = d.pop("primary", UNSET)

        error = d.pop("error", UNSET)

        date_force_timezone = d.pop("date_force_timezone", UNSET)

        def _parse_date_time_format(data: object) -> Union[DateTimeFormatEnum, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                _date_time_format_type_0 = data
                date_time_format_type_0: Union[Unset, DateTimeFormatEnum]
                if isinstance(_date_time_format_type_0, Unset):
                    date_time_format_type_0 = UNSET
                else:
                    date_time_format_type_0 = DateTimeFormatEnum(_date_time_format_type_0)

                return date_time_format_type_0
            except:  # noqa: E722
                pass
            return cast(Union[DateTimeFormatEnum, None, Unset], data)

        date_time_format = _parse_date_time_format(d.pop("date_time_format", UNSET))

        date_include_time = d.pop("date_include_time", UNSET)

        date_show_tzinfo = d.pop("date_show_tzinfo", UNSET)

        def _parse_number_decimal_places(data: object) -> Union[None, NumberDecimalPlacesEnum, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, int):
                    raise TypeError()
                _number_decimal_places_type_0 = data
                number_decimal_places_type_0: Union[Unset, NumberDecimalPlacesEnum]
                if isinstance(_number_decimal_places_type_0, Unset):
                    number_decimal_places_type_0 = UNSET
                else:
                    number_decimal_places_type_0 = NumberDecimalPlacesEnum(_number_decimal_places_type_0)

                return number_decimal_places_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, NumberDecimalPlacesEnum, Unset], data)

        number_decimal_places = _parse_number_decimal_places(d.pop("number_decimal_places", UNSET))

        def _parse_array_formula_type(data: object) -> Union[ArrayFormulaTypeEnum, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                _array_formula_type_type_0 = data
                array_formula_type_type_0: Union[Unset, ArrayFormulaTypeEnum]
                if isinstance(_array_formula_type_type_0, Unset):
                    array_formula_type_type_0 = UNSET
                else:
                    array_formula_type_type_0 = ArrayFormulaTypeEnum(_array_formula_type_type_0)

                return array_formula_type_type_0
            except:  # noqa: E722
                pass
            return cast(Union[ArrayFormulaTypeEnum, None, Unset], data)

        array_formula_type = _parse_array_formula_type(d.pop("array_formula_type", UNSET))

        def _parse_date_format(data: object) -> Union[DateFormatEnum, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                _date_format_type_0 = data
                date_format_type_0: Union[Unset, DateFormatEnum]
                if isinstance(_date_format_type_0, Unset):
                    date_format_type_0 = UNSET
                else:
                    date_format_type_0 = DateFormatEnum(_date_format_type_0)

                return date_format_type_0
            except:  # noqa: E722
                pass
            return cast(Union[DateFormatEnum, None, Unset], data)

        date_format = _parse_date_format(d.pop("date_format", UNSET))

        through_field_id = d.pop("through_field_id", UNSET)

        through_field_name = d.pop("through_field_name", UNSET)

        target_field_id = d.pop("target_field_id", UNSET)

        target_field_name = d.pop("target_field_name", UNSET)

        _formula_type = d.pop("formula_type", UNSET)
        formula_type: Union[Unset, FormulaTypeEnum]
        if isinstance(_formula_type, Unset):
            formula_type = UNSET
        else:
            formula_type = FormulaTypeEnum(_formula_type)

        lookup_field_field_serializer_with_related_fields = cls(
            id=id,
            table_id=table_id,
            name=name,
            order=order,
            type=type,
            read_only=read_only,
            related_fields=related_fields,
            nullable=nullable,
            primary=primary,
            error=error,
            date_force_timezone=date_force_timezone,
            date_time_format=date_time_format,
            date_include_time=date_include_time,
            date_show_tzinfo=date_show_tzinfo,
            number_decimal_places=number_decimal_places,
            array_formula_type=array_formula_type,
            date_format=date_format,
            through_field_id=through_field_id,
            through_field_name=through_field_name,
            target_field_id=target_field_id,
            target_field_name=target_field_name,
            formula_type=formula_type,
        )

        lookup_field_field_serializer_with_related_fields.additional_properties = d
        return lookup_field_field_serializer_with_related_fields

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
