import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union

import attr

from ..models.events_3_ea_enum import Events3EaEnum
from ..models.request_method_enum import RequestMethodEnum
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.table_webhook_create_request_headers import TableWebhookCreateRequestHeaders


T = TypeVar("T", bound="TableWebhookCreateRequest")


@attr.s(auto_attribs=True)
class TableWebhookCreateRequest:
    """
    Attributes:
        url (str): The URL that must be called when the webhook is triggered.
        name (str): An internal name of the webhook.
        include_all_events (Union[Unset, bool]): Indicates whether this webhook should listen to all events.
        events (Union[Unset, List[Events3EaEnum]]): A list containing the events that will trigger this webhook.
        request_method (Union[Unset, RequestMethodEnum]):
        headers (Union[Unset, TableWebhookCreateRequestHeaders]): The additional headers as an object where the key is
            the name and the value the value.
        use_user_field_names (Union[Unset, bool]): Indicates whether the field names must be used as payload key instead
            of the id.
    """

    url: str
    name: str
    include_all_events: Union[Unset, bool] = UNSET
    events: Union[Unset, List[Events3EaEnum]] = UNSET
    request_method: Union[Unset, RequestMethodEnum] = UNSET
    headers: Union[Unset, "TableWebhookCreateRequestHeaders"] = UNSET
    use_user_field_names: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        url = self.url
        name = self.name
        include_all_events = self.include_all_events
        events: Union[Unset, List[str]] = UNSET
        if not isinstance(self.events, Unset):
            events = []
            for events_item_data in self.events:
                events_item = events_item_data.value

                events.append(events_item)

        request_method: Union[Unset, str] = UNSET
        if not isinstance(self.request_method, Unset):
            request_method = self.request_method.value

        headers: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.headers, Unset):
            headers = self.headers.to_dict()

        use_user_field_names = self.use_user_field_names

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "url": url,
                "name": name,
            }
        )
        if include_all_events is not UNSET:
            field_dict["include_all_events"] = include_all_events
        if events is not UNSET:
            field_dict["events"] = events
        if request_method is not UNSET:
            field_dict["request_method"] = request_method
        if headers is not UNSET:
            field_dict["headers"] = headers
        if use_user_field_names is not UNSET:
            field_dict["use_user_field_names"] = use_user_field_names

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        url = self.url if isinstance(self.url, Unset) else (None, str(self.url).encode(), "text/plain")
        name = self.name if isinstance(self.name, Unset) else (None, str(self.name).encode(), "text/plain")
        include_all_events = (
            self.include_all_events
            if isinstance(self.include_all_events, Unset)
            else (None, str(self.include_all_events).encode(), "text/plain")
        )
        events: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.events, Unset):
            _temp_events = []
            for events_item_data in self.events:
                events_item = events_item_data.value

                _temp_events.append(events_item)
            events = (None, json.dumps(_temp_events).encode(), "application/json")

        request_method: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.request_method, Unset):
            request_method = (None, str(self.request_method.value).encode(), "text/plain")

        headers: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.headers, Unset):
            headers = (None, json.dumps(self.headers.to_dict()).encode(), "application/json")

        use_user_field_names = (
            self.use_user_field_names
            if isinstance(self.use_user_field_names, Unset)
            else (None, str(self.use_user_field_names).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "url": url,
                "name": name,
            }
        )
        if include_all_events is not UNSET:
            field_dict["include_all_events"] = include_all_events
        if events is not UNSET:
            field_dict["events"] = events
        if request_method is not UNSET:
            field_dict["request_method"] = request_method
        if headers is not UNSET:
            field_dict["headers"] = headers
        if use_user_field_names is not UNSET:
            field_dict["use_user_field_names"] = use_user_field_names

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.table_webhook_create_request_headers import TableWebhookCreateRequestHeaders

        d = src_dict.copy()
        url = d.pop("url")

        name = d.pop("name")

        include_all_events = d.pop("include_all_events", UNSET)

        events = []
        _events = d.pop("events", UNSET)
        for events_item_data in _events or []:
            events_item = Events3EaEnum(events_item_data)

            events.append(events_item)

        _request_method = d.pop("request_method", UNSET)
        request_method: Union[Unset, RequestMethodEnum]
        if isinstance(_request_method, Unset):
            request_method = UNSET
        else:
            request_method = RequestMethodEnum(_request_method)

        _headers = d.pop("headers", UNSET)
        headers: Union[Unset, TableWebhookCreateRequestHeaders]
        if isinstance(_headers, Unset):
            headers = UNSET
        else:
            headers = TableWebhookCreateRequestHeaders.from_dict(_headers)

        use_user_field_names = d.pop("use_user_field_names", UNSET)

        table_webhook_create_request = cls(
            url=url,
            name=name,
            include_all_events=include_all_events,
            events=events,
            request_method=request_method,
            headers=headers,
            use_user_field_names=use_user_field_names,
        )

        table_webhook_create_request.additional_properties = d
        return table_webhook_create_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
