import json
from typing import Any, Dict, List, Type, TypeVar, cast

import attr

T = TypeVar("T", bound="OrderPages")


@attr.s(auto_attribs=True)
class OrderPages:
    """
    Attributes:
        page_ids (List[int]): The ids of the pages in the order they are supposed to be set in
    """

    page_ids: List[int]
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        page_ids = self.page_ids

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "page_ids": page_ids,
            }
        )

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        _temp_page_ids = self.page_ids
        page_ids = (None, json.dumps(_temp_page_ids).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "page_ids": page_ids,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        page_ids = cast(List[int], d.pop("page_ids"))

        order_pages = cls(
            page_ids=page_ids,
        )

        order_pages.additional_properties = d
        return order_pages

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
